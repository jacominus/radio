#include <Metro.h>
#include <AltSoftSerial.h>
#include <wavTrigger.h>
#include <ResponsiveAnalogRead.h>

// Capteurs
#define STATION_ANALOG_PIN A0
#define BUTTON_PIN 2

#define NB_TRACKS 4
#define VOLMIN   -35

//void play_all_tracks_in_loop(void);
void init_track_loops(void);

//Audio
wavTrigger wTrig;   // PIN RX-9 (TX-8)

//Paramètres =====================

int bandeLargeur = 30, bandeThresh = 20, bandeRaideur = 1;
//int bandeLargeur = 100, bandeThresh = 50, bandeRaideur = 1;

//int bandes[NB_TRACKS] = {100, 300, 500, 700, 900};
int bandes[NB_TRACKS] = {100, 367, 634, 901};

bool track_loops[NB_TRACKS + 1] = {true, false, false, false, true};
int volume[NB_TRACKS + 1] = {VOLMIN, VOLMIN, VOLMIN, VOLMIN, -50};

bool mute = false;
bool firstLoop = true;

// ===============================

//Analogique
ResponsiveAnalogRead stationValue;
//ResponsiveAnalogRead volumeValue;

//Démarrage
void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  //Attente de 2 secondes de sécurité et sérial de débug
  delay(2000);
  Serial.begin(115200);

  //Démarrage des modules
  Serial.println("Démarrage");

  //Analogique
  //analogReference(EXTERNAL);
  stationValue = ResponsiveAnalogRead(STATION_ANALOG_PIN, true);stationValue.update();
  //volumeValue = ResponsiveAnalogRead(VOLUME_ANALOG_PIN, true);

  //Démarrage de l'audio
  wTrig.start();
  delay(10);
  wTrig.setAmpPwr(false);
  wTrig.stopAllTracks();
  wTrig.samplerateOffset(0);
  wTrig.masterGain(0);
  //wTrig.masterGain(map(volumeValue.getValue(), 0, 1023, -70, 0));

  //play_all_tracks_in_loop();
  init_track_loops();
  //wTrig.trackStop(6);
}

//Boucle
void loop() {
  if (!digitalRead(BUTTON_PIN)) {
    //mute = !mute;
    switch (mute = !mute) {
      case true:
        for (int i = 0 ; i < (NB_TRACKS + 1) ; i ++) wTrig.trackPause(i + 1);
        break;
      case false:
        wTrig.resumeAllInSync();
        break;
      default:
        // No statements
        break;
    }
    delay(500);
  }

  // Actualisation des stations
  stationValue.update();
  int station = stationValue.getValue();

  for(int i = 0; i < NB_TRACKS; i++) {
    if(station > (bandes[i] - 64) && track_loops[i] == false) {
      int track = i + 1;
      wTrig.trackPlayPoly(track);
      wTrig.trackGain(track, VOLMIN);
      Serial.print("Joue ");
      Serial.println(track);
      track_loops[i] = true;
    }
  }

  if ((firstLoop) || (stationValue.hasChanged())) {
    Serial.print(station);
    Serial.print("\t");
    int valMax = VOLMIN;
    for (int i = 0 ; i < NB_TRACKS ; i ++) {
      int track = i + 1;
      volume[i] = constrain(-(abs(bandes[i] - station) * bandeRaideur - bandeThresh), VOLMIN, 0);
      valMax = max(valMax, volume[i]);

      Serial.print(volume[i]);
      Serial.print("\t");
    }

    //volume[NB_TRACKS] = map(-valMax + VOLMIN, VOLMIN, 0, VOLMIN / 2, 0);
    volume[NB_TRACKS] = map(-valMax + VOLMIN, VOLMIN, 0, VOLMIN * 3 / 4, -6);
    Serial.print("\t");
    Serial.print(volume[NB_TRACKS]);
    Serial.print("\t");
    Serial.println("");

    //Applique le volume
    for (int i = 0 ; i < (NB_TRACKS + 1) ; i ++) {
      int track = i + 1;
      wTrig.trackGain(track, volume[i]);
    }

    firstLoop = false;
  }
}

//void play_all_tracks_in_loop(void) {
//  //Lance les sons en boucle
//  for (int i = 0 ; i < (NB_TRACKS + 1) ; i ++) {
//    int track = i + 1;
//    wTrig.trackPlayPoly(track);
//    wTrig.trackGain(track, volume[i]);
//    wTrig.trackLoop(track, 1);
//    Serial.print("Joue ");
//    Serial.println(track);
//  }
//}

void init_track_loops(void) {
  while(stationValue.getValue() > 10) {
    stationValue.update();
  }
  //Lance les sons en boucle
  for (int i = 0 ; i < (NB_TRACKS + 1) ; i ++) {
    if (track_loops[i] == true) {
      int track = i + 1;
      wTrig.trackPlayPoly(track);
      wTrig.trackGain(track, volume[i]);
      wTrig.trackLoop(track, 1);
      Serial.print("Joue ");
      Serial.println(track);
    }
  }
}
